const mongoose = require("mongoose");

const userSchema = new mongoose.Schema(
  {
   firstName: {
      type: String,
      required:[true, "First name is required"]
   },
   lastName: {
      type: String,
      required: [true, "Last name is required"]
   },
   email: {
      type: String,
      required: [true, "Email is required"]
   },
   password: {
      type: String,
      required: [true, "Password is required"]
   },
   isAdmin: {
      type: Boolean,
      default: false
   },
   mobileNumber: {
      type: String,
      required: [true, "Number is required"]
   },
   orders: [
	    {
      totalAmount: {
        type: Number,
        required: [true, "Total amount is required"]
      },
      purchasedOn:{ 
        type: Date,
        default: new Date()
      },
      products: [
  
        {
          productId:{
            type: String,
            required: [true, "Product Id is required"]
          },
          productName: {
            type: String,
            required: [false]
          },
          quantity:{
            type: Number,
            required: [true, "Quantity is required"]
          }
        }
  
      ]
    }
  
  ]
  
   
  }
)


module.exports = mongoose.model("user", userSchema)
