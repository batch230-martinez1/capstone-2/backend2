const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers.js");
const auth = require("../auth.js");

//  Create Product
router.post("/create", auth.verify,(request, response) =>{
  const newProduct ={
    product: request.body,
    isAdmin: auth.decode(request.headers.authorization).isAdmin
  }
  productControllers.addProduct(newProduct).then(resultFromController => response.send(resultFromController))
})

// Get all product 
router.get("/all", auth.verify, productControllers.getAllProducts);

// Get all active products
router.get("/active", (request, response) =>{
  productControllers.getActiveProducts().then(resultFromController => response.send(resultFromController))
})

// update product
router.put("/:productId/update", auth.verify,(request,response)=> {
  const newData = {
  product: request.body, 	
  //request.headers.authorization contains jwt
  isAdmin: auth.decode(request.headers.authorization).isAdmin
}

  productControllers.updateProduct(request.params.productId, newData).then(resultFromController => {
  response.send(resultFromController)
})
})

// retrieving single product
router.get("/:productId", productControllers.getProduct);


// update a product to active or not active
router.put("/:productId/archive", auth.verify,(request,response)=> {
  const newData = {
  product: request.body, 	

  isAdmin: auth.decode(request.headers.authorization).isAdmin
}

// archive product
  productControllers.archiveProduct(request.params.productId, newData).then(resultFromController => {
  response.send(resultFromController)
})
})
// filter product by name

router.post("/filter", productControllers.filterProductsByName);

// filter product by category
router.post("/category", productControllers.filterProductsByCategory);



module.exports = router;